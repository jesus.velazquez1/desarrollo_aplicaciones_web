***programacion***
5AVP
Jesús Manuel Velazquez Urtuzuastegui 

-Practica #1 - 02/09/2022 - Practica de ejemplo
Commit: cb3cccb226416e2097e1ab1b89e4f942523934b1
Archivo: https://gitlab.com/jesus.velazquez1/desarrollo_aplicaciones_web/-/blob/main/parcial%201/practica_ejemplo.html

-Practica #2 - 09/09/2022 - Practica JavaScript
Commit: ffea90a66ce1fee95003aafc03b661bbf1e3b197
Archivo: https://gitlab.com/jesus.velazquez1/desarrollo_aplicaciones_web/-/blob/main/parcial%201/practicaJavaScript.html

-Practica #3 - 15/09/2022 - Practica web con bases de datos-parte 1
Commit: 617fd3510838e3697e68e09c227d477cdfb4f577
Archivo: https://gitlab.com/jesus.velazquez1/desarrollo_aplicaciones_web/-/blob/main/parcial%201/PracticaWebDatos.rar

-Practica #4 - 19/09/2022 - Practica web con bases de datos - Vista de consulta de datos
Commit: 617fd3510838e3697e68e09c227d477cdfb4f577
Archivo:https://gitlab.com/jesus.velazquez1/desarrollo_aplicaciones_web/-/blob/main/parcial%201/PracticaWebDatos/consultarDatos.php

-Practica #5 - 19/09/2022 - Practica web con bases de datos - Vista de registro de datos
Commit: 617fd3510838e3697e68e09c227d477cdfb4f577
Archivo: https://gitlab.com/jesus.velazquez1/desarrollo_aplicaciones_web/-/blob/main/parcial%201/PracticaWebDatos/registrarDatos.html
